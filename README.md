# Music Managment Studio 

-----------------------------------------

## Descrição
* Ferramenta para gestão de músicas.
* Dados inseridos não são persistentes ao terminar o programa!

## Especificações
* Programado em Python v3.7.7;
* Controlador de versões Git;
* Desenvolvido com Visual Studio Code;
* Otimizado para Sistemas Operativos Linux.

### Instruções de configuração Docker
1. Build da imagem python _docker build -t img:v2 ._ ;
2. Run do container _docker run -it -d --name equipa1 img:v2 sh_ ;
3. Executar o container _docker exec -it equipa1 sh_ ;
4. Dentro do container _python3 main.py_ .


### Instruções de Configuração

#### Windows
1. Instalar o python através do site (https://www.python.org/downloads/), e seguir as respectivas instruções para Windows;
2. Fazer o download/clone do repositório para uma pasta;
3. Na linha de comandos executar _sudo apt install python-pip_ .

#### Linux
1. Instalar o python através do site (https://www.python.org/downloads/), e seguir as respectivas instruções para Linux;
2. Fazer o download/clone do repositório para uma pasta;
3. Na linha de comandos executar _sudo apt install python3-pip_ ;
4. Na linha de comandos executar _pip3 install termcolor_.


### Instruçoes de Execução

#### Windows
1. Após o ambiente estar configurado, abra uma linha de comandos (em modo administrador);
2. Navegar na linha de comandos até à diretoria do repositório;
3. Executar comando _python main.py_.

#### Linux
1. Após o ambiente estar configurado, abra o terminal;
2. Navegar no terminal até à diretoria do repositório;
3. Executar comando _python3 main.py_.

 
### Instruções de Utilização
* Apresentação do menu principal que permite navegar para os menus enunciados;
* No menu registar podes inserir músicas com nome e género;
* No menu consultar podes ver todas as múscias (nome e género) registadas;
* No menu editar podes alterar, através do ID, apenas o nome das músicas registadas;
* No menu eliminar podes remover, através do ID, as músicas registadas.

### Membros da Equipa
* Hélia Barroso  - Scrum Master
* João Martins  - Developer
* Patricia Machado  - Developer
* Samuel Ferreira  - Developer

### Funcionalidades 

#### Versão 1.0
* F01 - Apresentar menus e respectivas ligações;
* F02 - Registar musicas;
* F03 - Editar musicas;
* F04 - Eliminar musicas;
* F05 - Listar musicas.

#### Versão 2.0
* CR01 - Code Refactor;
* F06 - Listar Ordem Alfabética; 
* F07 - Eliminação da Música;
* F08 - Array Géneros;
* F11 Log com Cores;
* HF01 - Erro Editar Música.

### Gestão do Projecto
* Integração do Trello com Bitbucket

### Fim
