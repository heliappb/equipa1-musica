# IMPORTS______________________________________________________________

import sys 
from termcolor import colored, cprint

# VARIABLES____________________________________________________________

musicas = []
generos = []

# METHODS______________________________________________________________

def printMenuPrincipal():

    print('# --------------------------- #')
    print('#        MENU PRINCIPAL       #')
    print('# --------------------------- #')
    print()
    print('1 - Registar Músicas')
    print('2 - Consultar Músicas')
    print('3 - Editar Músicas')
    print('4 - Eliminar Músicas')
    print('0 - Terminar')
    print()

    opcao = int(input("Opção: "))

    if opcao == 1:
        printMenuRegistar()
    elif opcao == 2:
        printMenuMusicas()
    elif opcao == 3:
        printEditarMusicas()
    elif opcao == 4:
        printEliminarMusicas()
    elif opcao == 0:
        print()
        print(colored('PROGRAMA TERMINADO!', 'green'))
        print()
        sys.exit()
    else:
        print('Insira um número válido')

def printMenuRegistar():

    print('# --------------------------- #')
    print('#        MENU REGISTAR        #')
    print('# --------------------------- #')
    print()
    nome = input("Música: ")

    musicas.append(nome)
    print()

    tipo = input("Género: ")

    generos.append(tipo)
    print()

    print(colored('MÚSICA REGISTADA!', 'green'))
    print()

    printMenuPrincipal()

def printMenuMusicas():

    print()
    print('# --------------------------- #')
    print('#        MENU CONSULTA        #')
    print('# --------------------------- #')
    print()
    print('1 - Ordenação alfabética')
    print('2 - Ordenação aleatória')
    print()

    opcao = int(input("Opção: "))
    print()
    if opcao == 1:
        j = 0 
        
        for i in sorted(musicas):
            print(str(j+1) + ' - ' + i)            
            j = j + 1
    elif opcao == 2:
        if len(musicas) > 0 and len(generos) > 0:
            k = 0
            for i in musicas: 
                print(str(k+1) + ' - ' + musicas[k] + " - " + generos[k])
                k = k + 1 
        else:
            print('NÃO EXISTEM MUSICAS REGISTADAS...')

    print()
    printMenuPrincipal() 

def printEditarMusicas():
    print()
    print('# --------------------------- #')
    print('#         MENU EDITAR         #')
    print('# --------------------------- #')
    print()

    if len(musicas) > 0:
        musica_id = int(input("ID: "))-1
        print()

        if musica_id < len(musicas):
            musica_nome = input("Música: ")
            print()

            musicas[musica_id] = musica_nome

            print(colored('MÚSICA EDITADA!', 'green'))
            print()

        else:
            print()
            print(colored('ID INSERIDO NÃO EXISTE...', 'red'))
            print()
            printMenuPrincipal()            
    else:
        print(colored('SEM MÚSICAS...', 'red'))
    
    print()
    printMenuPrincipal()    

def printEliminarMusicas():
    print()
    print('# --------------------------- #')
    print('#        MENU ELIMINAR        #')
    print('# --------------------------- #')
    print()

    if len(musicas) > 0:
        musica_id = int(input("ID: "))-1
        print()

        if musica_id < len(musicas): 

            check = input("Tem a certeza? (y/n): ")
            print()

            if check == 'y':
                del musicas[musica_id]
                del generos[musica_id]

                print(colored('MÚSICA ELIMINADA!', 'green'))
            else:
                print(colored('CANCELAR...', 'red'))

        else:
            print()
            print(colored('ID INSERIDO NÃO EXSITE...', 'red'))
            print()
            printMenuPrincipal()            
    else:
        print(colored('SEM MÚSICAS...', 'red'))
    
    print()
    printMenuPrincipal()  
    

# MAIN_________________________________________________________________

# SCREEN CLEANER
print(chr(27) + "[2J")

printMenuPrincipal()
